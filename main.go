package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/golang-training2/udemy-course-api/database"
	"gitlab.com/golang-training2/udemy-course-api/middleware"
	"gitlab.com/golang-training2/udemy-course-api/service/course"
)

func main() {
	if err := database.Connect(); err != nil {
		log.Fatal(err)
	}

	app := fiber.New()
	app.Use(middleware.SetAuthUser())

	app.Get("courses/:id", course.GetCourseDetail)
	auth := app.Group("", middleware.BasicAuth())
	auth.Get("/", func(c *fiber.Ctx) error {
		err := c.JSON(fiber.Map{
			"status": "ok",
		})
		return err
	})

	log.Fatal(app.Listen("192.168.18.209:3000"))
}
