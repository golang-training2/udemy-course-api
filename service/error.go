package service

type ErrorResponse struct {
	Error ErrorJson `json:"error"`
}

type ErrorJson struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
