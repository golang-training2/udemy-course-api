package course

import (
	"database/sql"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/lib/pq"
	"gitlab.com/golang-training2/udemy-course-api/database"
	"gitlab.com/golang-training2/udemy-course-api/middleware"
	"gitlab.com/golang-training2/udemy-course-api/service"
)

type Course struct {
	ID        string   `json:"id"`
	Title     string   `json:"title"`
	Subtitle  string   `json:"subtitle"`
	Creator   string   `json:"creator"`
	Language  string   `json:"language"`
	Learns    []string `json:"learns"`
	Price     int      `json:"price"`
	CreatedAt string   `json:"createdAt"`
	UpdatedAt string   `json:"updatedAt"`
}

func GetCourseDetail(c *fiber.Ctx) error {
	var response interface{}
	id := c.Params("id")
	row := database.Db.QueryRow("SELECT * FROM courses where id = $1", id)
	course := Course{}
	err := row.Scan(&course.ID, &course.Title, &course.Subtitle, &course.Creator, &course.Language, (*pq.StringArray)(&course.Learns), &course.Price, &course.CreatedAt, &course.UpdatedAt)

	switch {
	case err == sql.ErrNoRows:
		response = service.ErrorResponse{
			Error: service.ErrorJson{
				Code:    404,
				Message: "Course Not Found!",
			},
		}
	case err != nil:
		response = service.ErrorResponse{
			Error: service.ErrorJson{
				Code:    500,
				Message: err.Error(),
			},
		}
	default:
		response = course
	}
	fmt.Println(middleware.AuthUser)
	return c.JSON(response)
}
